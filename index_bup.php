<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Radiowealth Finance Corporation - Loan Registration</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/wizard.css" rel="stylesheet" />

	<link href="assets/css/styles.css" rel="stylesheet" />
</head>

<body>
	<div class="image-container set-full-height" style="background-image: url('assets/img/rfc-bg-image.jpg')">

	    <!--   Big container   -->
	    <div class="container">
	    	<div class="row">
			<div class="hidden-xs col-sm-5 col-sm-offset-7">
				<div class="social-icons">
					<ul class="list-inline">
						<li><a type="button" class="btn-floating btn-fb" href="https://www.facebook.com/radiowealthofficial/"><i class="fa fa-facebook"></i></a></li>
						<li><a type="button" class="btn-floating btn-tw" href="https://twitter.com/RFC_Inc"><i class="fa fa-twitter"></i></a></li>
						<li><a type="button" class="btn-floating btn-ins" href="https://www.instagram.com/radiowealthofficial/"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="blue" id="wizard">
							<div class="hidden-xs text-center">
								<img class="logo img-responsive" src="assets/img/logo.png" alt="rfc logo" />
							</div>
							<div class="hidden-sm hidden-md hidden-lg">
									<img class="logo-xs img-reponsive pull-left" src="assets/img/rfc-xs.jpeg" alt="rfc logo" />
									<div class="social-icons">
												<a type="button" class="btn-floating btn-fb" href="https://www.facebook.com/radiowealthofficial/"><i class="fa fa-facebook"></i>
												</a>
												<a type="button" class="btn-floating btn-tw" href="https://twitter.com/RFC_Inc"><i class="fa fa-twitter"></i>
												</a>
												<a type="button" class="btn-floating btn-ins" href="https://www.instagram.com/radiowealthofficial/"><i class="fa fa-instagram"></i>
												</a>
									</div>
							</div>
								
							<div class="clearfix"></div>
			                <form method="post" action="success.php">			                
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title">Ready. Fast. Cash.</h3>
		                    	</div>
								<div class="wizard-navigation">
									<ul>
			                            <li><a href="#one" data-toggle="tab">How Can We Help</a></li>
			                            <li><a href="#two" data-toggle="tab">Personal Information</a></li>
			                            <li><a href="#three" data-toggle="tab">Employment and Finance</a></li>
			                        </ul>
								</div>

		                        <div class="tab-content">
		                            <div class="tab-pane" id="one">
		                            	<div class="row">
		                                	<div class="col-sm-12">
		                                    	<h4 class="info-text">We're here to help.</h4>
		                                	</div>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Type of Loan</label>
	                                            	<select id="loan-type" name="loan-type" class="form-control" required>
	                                                	<option disabled="" selected=""></option>
	                                                	<option value="appliance"> Appliance </option>
                                                        <option value="personal-vehicle"> Personal Vehicle </option>
                                                        <option value="public-utility-vehicle"> Public Utility Vehicle </option>
                                                        <option value="heavy-equipment-farming-machineries"> Heavy Equipment/Farming Machineries </option>
                                                        <option value="truck"> Truck </option>
														<option value="special-equipment"> Special Equipment </option>
														<option value="real-estate-housing-loan"> Real Estate/Housing Loan </option>
														<option value="bridge-financing"> Bridge Financing </option>
														<option value="receivable"> Receivable </option>
														<option value="inventory"> Inventory </option>
														<option value="working-capital"> Working Capital </option>
														<option value="capex"> Capex </option>
														<option value="salary-loan"> Salary Loan </option>
														<option value="professional-loan"> Professional Loan </option>
														<option value="ofw-loan"> OFW Loan </option>
														<option value="personal-loan"> Personal Loan </option>
														<option value="education-loan"> Education Loan </option>
														<option value="equity-loan"> Equity Loan </option>
														<option value="home-asset-loan"> Home/Asset Loan </option>
														<option value="take-out-loan"> Take-Out Loan </option>														
	                                            	</select>
	                                        	</div>
		                                	</div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Sub Type</label>
	                                            	<select id="sub-type" name="sub-type" class="form-control" required>
														<option disabled="" selected=""></option>
													</select>
	                                        	</div>
		                                	</div>																						
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Desired Loan (Min. Loan of 10,000)</label>	                                        	
		                                            <input type="text" name="desired-loan" class="form-control int" required>
		                                    	</div>
		                                	</div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Prefered Payment Term</label>
	                                            	<select name="payment-term" class="form-control" required>
	                                                	<option disabled="" selected=""></option>
	                                                	<option value="monthly"> Monthly </option>
	                                                	<option value="quarterly"> Quarterly </option>
	                                                	<option value="annually"> Annually </option>
	                                            	</select>
	                                        	</div>
		                                	</div>
		                                	<div class="col-sm-10 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">What is the purpose of your loan application?</label>		                                        	
		                                            <input type="text" name="purpose" class="form-control" required>          
                                                    
                                                </div>
		                                	</div>
		                            	</div>
		                            </div>
		                            <div class="tab-pane" id="two">
		                                <h4 class="info-text">Let us know all about you. </h4>
		                                <div class="row">
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Last Name</label>
		                                        	<input type="text" name="last-name" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">First Name</label>
		                                        	<input type="text" name="first-name" class="form-control" >
		                                    	</div>
                                            </div> 
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Middle Name</label>
		                                        	<input type="text" name="middle-name" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Suffix</label>
		                                        	<input type="text" name="suffix" class="form-control" >
		                                    	</div>
                                            </div> 
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Gender</label>
	                                            	<select name="gender" class="form-control">
	                                                	<option disabled="" selected=""></option>
	                                                	<option value="male"> Male </option>
	                                                	<option value="female"> Female </option>
	                                            	</select>
	                                        	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Date of birth (mm/dd/yyyy)</label>
		                                        	<input type="text" name="dob" class="form-control date-in" >
		                                    	</div>
                                            </div> 
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Place of birth</label>
		                                        	<input type="text" name="place-of-birth" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Primary email address</label>
		                                        	<input name="email" type="email" class="form-control"  value="">
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Active landline number</label>
		                                        	<input type="text" name="phone" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Active mobile number</label>
		                                        	<input type="text" name="mobile" class="form-control mob">
		                                    	</div>
                                            </div> 
		                                	<div class="col-sm-10 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Present residential address</label>
		                                        	<input type="text" name="present-address" class="form-control" >
		                                    	</div>
                                            </div>   
		                                	<div class="col-sm-2 col-sm-offset-1">
                                                <div class="checkbox float-left">
                                                    <label>
                                                        <input type="checkbox" name="ps-rented" value="rented">
                                                        Rented
                                                    </label>
                                                </div>  
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="checkbox float-left">
                                                    <label>
                                                        <input type="checkbox" name="ps-owned" value="owned">
                                                        Owned
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ps-owned-mortgaged" value="owned-mortgaged">
                                                        Owned-Mortgaged
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="ps-with-relatives-parents" value="with-relatives">
                                                        With relatives/parents
                                                    </label>
                                                </div>                                                                                                                                                
                                            </div>   
		                                	<div class="col-sm-10 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Permanent residential address</label>
		                                        	<input type="text" name="permanent-address" class="form-control" >
		                                    	</div>
                                            </div>   
		                                	<div class="col-sm-2 col-sm-offset-1">
                                                <div class="checkbox float-left">
                                                    <label>
                                                        <input type="checkbox" name="pm-rented" value="rented">
                                                        Rented
                                                    </label>
                                                </div>  
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="checkbox float-left">
                                                    <label>
                                                        <input type="checkbox" name="pm-owned" value="owned">
                                                        Owned
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="pm-owned-mortgaged" value="owned-mortgaged">
                                                        Owned-Mortgaged
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="pm-with-relatives-parents" value="with-relatives">
                                                        With relatives/parents
                                                    </label>
                                                </div>                                                                                                                                                
                                            </div>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">SSS/GSIS number</label>
		                                        	<input type="text" name="sss-gsis" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">TIN number</label>
		                                        	<input type="text" name="tin" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Nationality</label>
		                                        	<input type="text" name="nationality" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Civil status</label>
	                                            	<select id="select-civil" name="civil-status" class="form-control">
	                                                	<option disabled="" selected=""></option>
	                                                	<option value="married"> Married </option>
                                                        <option value="widowed"> Widowed </option>
                                                        <option value="separated"> Separated </option>
                                                        <option value="divorced"> Divorced </option>
                                                        <option value="single"> Single </option>
	                                            	</select>
	                                        	</div>
                                            </div>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                            <label class="control-label">Highest educational attainment</label>
	                                            	<select name="education" class="form-control">
	                                                	<option disabled="" selected=""></option>
	                                                	<option value="hs-graduate"> High school graduate </option>
                                                        <option value="college-nd"> Some college, no degree </option>
                                                        <option value="bsd"> Bachelor's degree </option>
                                                        <option value="msd"> Master's degree </option>
                                                        <option value="phd"> Doctorate degree </option>
	                                            	</select>
                                            	</div>
                                            </div>
		                                	<div class="col-sm-5 civil-married">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Spouse Last Name</label>
		                                        	<input type="text" name="spouse-last-name" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5 col-sm-offset-1 civil-married">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Spouse First Name</label>
		                                        	<input type="text" name="spouse-first-name" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5 civil-married">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Spouse Middle Name</label>
		                                        	<input type="text" name="spouse-middle-name" class="form-control" >
		                                    	</div>
                                            </div>  
		                                	<div class="col-sm-5 col-sm-offset-1 civil-married">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Suffix</label>
		                                        	<input type="text" name="spouse-suffix" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5 civil-married">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Spouse active landline/mobile number</label>
		                                        	<input type="text" name="spouse-phone" class="form-control" >
		                                    	</div>
                                            </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
		                                </div>
		                            </div>
		                            <div class="tab-pane" id="three">
		                                <div class="row">
		                                    <h4 class="info-text"> Employment and Finance. </h4>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Employment Status</label>
	                                            	<select id="employment-status" name="employment-status" class="form-control" required>
	                                                	<option disabled="" selected=""></option>
	                                                	<option value="employed"> Employed </option>
                                                        <option value="self-employed"> Self Employed </option>												
	                                            	</select>
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Company Name</label>
		                                        	<input type="text" name="company-name" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Position</label>
		                                        	<input type="text" name="company-position" class="form-control" >
		                                    	</div>
                                            </div>
		                                	<div class="col-sm-5">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Monthly Income</label>
		                                        	<input type="text" name="monthly-income" class="form-control int" >
		                                    	</div>
                                            </div> 
		                                	<div class="col-sm-5 col-sm-offset-1">
		                                    	<div class="form-group label-floating">
		                                        	<label class="control-label">Years of Employment</label>
		                                        	<input type="text" name="years-employment" class="form-control" >
		                                    	</div>
                                            </div>                                                                                       
		                                </div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
	                            	<div class="pull-right">
	                                    <input type='button' class='btn btn-next btn-fill btn-primary btn-wd' name='next' value='Get It Now' />
	                                    <input name="submit" type='submit' class='btn btn-finish btn-fill btn-primary btn-wd' value='Finish' />
	                                </div>
	                                <div class="pull-left">
	                                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Previous' />
	                                </div>
		                            <div class="clearfix"></div>
		                        </div>
			                </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	        </div> <!-- row -->
	    </div> <!--  big container -->

	    <div class="footer">
	        <div class="container text-center">
	        <p><a data-toggle="modal" data-target="#terms" href="#">Terms and Condition</a> | <a data-toggle="modal" data-target="#privacy" href="#" target="_blank">Privacy Policy</a></p>
	        </div>
	    </div>

	    <!-- Modal -->
			<div id="terms" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Terms and Condition</h4>
			      </div>
			      <div class="modal-body">
						<p>This website is owned by Radiowealth Finance Company (“RFC”), located at the DMG Center, 7th and 8th Floor D.M. Guevara Street, Mandaluyong City, Philippines.</p>

						<p>By using this website you agree to these terms and conditions. Please read carefully.</p>

						<p>For products and services, term conditions, requirements, and rates may vary from information posted on this website.</p>

						<h5><strong>Copyright</strong></h5>

						<p>All copyright and other intellectual property contained on this website is owned by or licensed to RFC. The information contained in this website may be used for your personal reference only. Information in this website may not otherwise be reproduced, distributed, or transmitted to any other person or entity, or may not in any way be reproduced in any other form.</p>

						 

						<h5><strong>Privacy Policies</strong></h5>

						<p>The RFC privacy policies explain how we shall treat your personal data and protect your privacy when you use our website. By using our website, you agree that RFC can use such data in accordance with our privacy policies.</p>

						 

						<h5><strong>Third Party Sites</strong></h5>

						<p>RFC is not responsible for the content or conditions in provided links of third party websites. Your access to third party websites and use of their products and services is solely at your own risk.</p>

				
						<h5><strong>Information on This Site</strong></h5>

						<p>We have carefully reviewed all information on this website, but some information on the website may still not be accurate, complete, or up to date</p>

						<p>Some contributed information may not be reflected here, since the website administrator reviews all information for relevance and truthfulness prior to posting on this website.</p>

						<p>Product information, requirements, costs, terms, conditions, and other information are subject to change at any time without prior notice.</p>

						<p>Information on this website does not intend to provide any financial, legal, or any other advice, nor does not take into account the financial circumstances of any person. Please seek appropriate professional advice before following any information on this site.</p>

						<h5><strong>Liability</strong></h5>

						<p>RFC is not liable to any person in relation to using this website. You also agree to waive all claims against RFC in relation to use of this website.</p>

						 

						<p>You indemnify RFC and its employees from any breach of these terms.</p>
                        
                        <p><strong>What  RFC Can Collect from You</strong><br />
                          </p>
                        <p style="text-align:justify">When you apply or use  any product or service RFC provides or when you engage with our employees,  authorized representatives, agents and providers, we collect your personal  information. This may include, among others, your name, signature, and personal  details such as contact details, address, date of birth, education; government  ID details; financial information (such as income, expense, investment, tax,  financial history and transaction, etc.); employment details; business  interests and property; images by CCTV and other similar devices installed in  our offices.<br />
                          </p>
                        <p style="text-align:justify">We may, whenever  required, prove or supplement this information to third-parties including  government regulators, judicial, administrative bodies, tax authorities or  competent jurisdictional courts and, in the process, obtain additional  information about you.<br />
                          </p>
                        <p><strong>How  We Use Your Information</strong><br />
                          </p>
                        <p style="text-align:justify">We use the information  collected to provide the products and services you have availed to process  applications and transactions; send you statements, notices and other documents  required for continued use of our products and services; conduct studies and research  for the purpose of developing our products and services; contact you about  product and service information; determine the effectiveness of our efforts and  marketing initiatives; comply with our processes, policies and procedures of  management, safekeeping, administrative, credit and risk management, policies  and procedures, the terms and conditions governing our products, services,  facilities, legal regulations of government, judicial, governing bodies or tax  authorities as both can be amended or supplemented from time to time; comply  with legal and regulatory requirements such as data submission to credit  offices, credit information companies, the Credit Information Corporation (CIC)  (pursuant to RA 9510 and the enforcement of regulations and regulations) in  court orders and other instructions and requests from any local or foreign  authorities including authorities, governments, tax and law enforcement  authorities or other similar authorities; perform other activities permitted by  law or with your consent.<br />
                          </p>
                        <p><strong>How  We Share Your Information</strong><br />
                          </p>
                        <p style="text-align:justify">We may share your  personal information with our subsidiaries, affiliates and third parties, under  a confidentiality obligation; We may share personal information with different  branches within RFC to better understand the way you use our products and  services. This will allow us to improve our services and offer you  opportunities to get such other useful products and services that can deliver  more value to you; We may share information with our subsidiaries and affiliates  to offer you additional products and services that we believe may be of  assistance to you; We may also share information in compliance with legal  requirements such as court orders; enforcing our terms of use with our rights  as a creditor to customers who use our loans or such other applicable policies  relating to the services we provide; response to fraud, security or technical  issues, to respond to an emergency or otherwise to protect the rights, property  or security of our customers or third parties; to perform all the other  purposes set forth above.<br />
                          </p>
                        <p><strong>How  We Protect Your Information </strong><br />
                          </p>
                        <p style="text-align:justify">We fully recognize the  value of your personal information which may include sensitive personal  information such as your gender, government issued IDs, etc. At the same time,  we strive to maintain confidentiality, integrity and availability of your  personal information through the use of physical, technological and security  techniques. We train our employees to properly handle your information.  Whenever we engage with other companies to provide services for us, we ask them  to protect personal information that is aligned with our own security  standards.<br />
                          </p>
                        <p><strong>How  Long Your Information Will Stay</strong><br />
                          </p>
                        <p style="text-align:justify">Your  personal information will remain as long as the purpose for which it was collected,  and other purposes with your consent remain valid and up to the time it is not  necessary or necessary to keep your information for any other legal, regulatory  or business goals.<br />
                          </p>
                        <p><strong>Know Your Rights </strong><br />
                          </p>
                        <p style="text-align:justify">With  respect to the processing of your personal data, you have the right to be  informed, to object to processing, to reasonable access, to rectification of  errors, to erasure, to damages when circumstances warrant it, and to data  portability subject to prior reasonable request from RFC.</p>
                        <p><strong>How  To Contact Us</strong><br />
                          For any questions,  clarifications or requests in any aspect of this Statement, please visit any of  our branches. You can also email us at dpo@rfc.com.ph<br />
                        </p>
                        <p>You can also write our  Data Privacy Officer to:<br />
                        </p>
                        <div style="margin-left:50px">
                            <p><strong>	DATA  PRIVACY OFFICER</strong><br />
                              7th Floor DMG Center<br />
                              DM Guevara Street, Brgy. Mauway<br />
                            Mandaluyong City</p>
                         </div>
                        <p><strong>Changes  in our Privacy Statement</strong></p>
                        <p style="text-align:justify">
                          We may change this  Privacy Statement from time to time to keep up with any changes to the laws and  regulations that apply to us or how we collect, use, protect, store, share, or  dispose of your personal information. Any updates will be posted in RFC website  at <a href="http://www.rfc.com.ph">www.rfc.com.ph</a></p>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>

			  </div>
			</div>
		<!-- Modal End -->
		<!-- Modal -->
			<div id="privacy" class="modal fade" role="dialog">
			  <div class="modal-dialog modal-lg">

			    <!-- Modal content-->
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal">&times;</button>
			        <h4 class="modal-title">Privacy Policy</h4>
			      </div>
			      <div class="modal-body">
						<p>We, Radiowealth Finance Company (RFC), are fully committed to protect and respect your privacy and personal information. Sharing your information to us will help us give you better services. This Privacy Policy shall explain the kind of information we collect, where said information will be used, and how you can protect your privacy.</p>

 

						<h5><strong>Information Collected</strong></h5>

						<p>We shall collect information such as your name, email address, contact information, and any other information that may be deemed necessary.</p>

						<p>We request this information for the following purposes:</p>

						<ul>
						
						<li>For your identification;</li>
						<li>For assessing your application;</li>
						<li>For attending to any requested inquiries and services</li>
						<li>For informing you promos, products, services we may offer; and</li>
						<li>For providing you information about RFC time to time.</li>
						</ul>

						<p>In some cases we may collect your personal information from other third parties such as your employer, credit bureau, or other publicly available sources of information.</p>

						 

						<h5><strong>Use and Disclosure</strong></h5>
						
						<p>All information given to us is used only for the purposes for which they were collected.</p>

						<p>Your personal information may then be disclosed to the following:</p>
						<ul>
						<li>Those involved in support and development of this website.</li>
						<li>RFC employees who shall attend to requested product inquiries and services.</li>
						<li>RFC employees who shall provide updates on promos, new products, and services, and other relevant information or updates about RFC.</li>
						<li>Credit and fraud reporting agencies, debt collection agencies, and organizations involved in our normal business practices.</li>
						<li>Agents or dealers who may referred you to us.</li>
						Government or regulatory bodies who may require any and all information for as long as they authorized by law.</li>
						<li>RFC’s partners and affiliates which, to our belief, have the capacity to offer you other products or services.</li>
						</ul>

						<h5><strong>Security</strong></h5>

						<p>RFC gives importance to security of your personal information, and we shall attend to your concern pertaining to any misuse, unauthorized access, and disclosure of your personal information.</p>

						 

						<p>We have implemented security and encryption technologies to protect your information. However, please be noted that any transmission of data over the Internet has no guarantee of complete security. We cannot give you 100% assurance to ensure your personal information security. The provision of your personal information to us is at your own risk; however, we shall take strict measures to protect your information.</p>

						 

						<h5><strong>Email</strong></h5>

						<p>We shall store any emails for our records, and giving these to us means you agree to do so.</p>

						 

						<h5><strong>Cookies</strong></h5>

						<p>RFC may use cookies for various purposes like security and enriching your web browsing experience. However, you can configure your browser so that it will not receive cookies.</p>

						 

						<p>Cookies shall not allow us to collect personally identifiable information about you.</p>

						 

						<h5><strong>Update on Privacy Policy and Terms of Use</strong></h5>

						<p>RFC may, at any given time, change and update the Privacy Policy and the Terms of Use, and you shall often see updates of said pages on the website. Please check these pages regularly.</p>


			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>

			  </div>
			</div>
		<!-- Modal End -->
	</div>

</body>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>

	<script type="text/javascript">
		$('.int').keyup(function(event) {
			// skip for arrow keys
			if(event.which >= 37 && event.which <= 40){
			event.preventDefault();
			}
		
			$(this).val(function(index, value) {
				value = value.replace(/,/g,''); // remove commas from existing input
				return numberWithCommas(value); // add commas back in
			});
		});
		
		function numberWithCommas(x) {
			var parts = x.toString().split(".");
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return parts.join(".");
		}	

      	$(".date-in").keyup(function(){
            if ($(this).val().length == 2){
                    $(this).val($(this).val() + "/");
            }else if ($(this).val().length == 5){
                  $(this).val($(this).val() + "/");
            }
		});	

        $(".mob").val("+63");	
		   	   	
	</script>

	<script type="text/javascript">
		$(document).ready(function () {
			$("#loan-type").change(function () {
				var val = $(this).val();
				if (val == "appliance") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='appliance-loan'>Appliance Loan</option><option value='appliance-loan-ofw'>Appliance Loan - OFW</option>");
				} else if (val == "personal-vehicle") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='personal-vehicle-loan'>Personal Vehicle Loan</option><option value='motorcycle-loan'>Motorcycle Loan</option>");
				} else if (val == "public-utility-vehicle") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='motor-tricycle'>Motor/Tricycle</option><option value='taxi'>Taxi</option><option value='jeepney'>Jeepney</option><option value='van'>Van/Shuttle/UV</option><option value='bus'>Bus</option><option value='commercial-boat'>Commercial Boat</option>");
				} else if (val == "heavy-equipment-farming-machineries") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='farming-machineries-equipment-loan'>Farming Machineries/Equipment Loan</option><option value='heavy-equipment-loan'>Heavy Equipment Loan</option>");
				} else if (val == "truck") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='truck-loan'>Truck Loan</option>");
				} else if (val == "special-equipment") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='medical-equipment-loan'>Medical Equipment Loan</option><option value='specialized-equipment-loan'>Specialized Equipment Loan</option>");
				} else if (val == "real-estate-housing-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='real-estate-housing-loan'>Real Estate/Housing Loan</option>");
				} else if (val == "bridge-financing") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='bridge-financing'>Bridge Financing</option>");
				} else if (val == "receivable") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='receivable-check-discounting'>Recievable/Check Discounting</option>");
				} else if (val == "inventory") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='inventory-financing-dealers-loan'>Inventory Financing - Dealer's Loan</option><option value='inventory-financing'>Inventory Financing</option>");
				} else if (val == "working-capital") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='working-capital-financing-franchise'>Working Capital Financing - Franchise</option><option value='working-capital-financing-wet-market'>Working Capital Financing - Wet Market</option><option value='working-capital-financing-unsecured'>Working Capital Financing - Unsecured</option><option value='working-capital-financing-secured'>Working Capital Financing - Secured</option><option value='working-capital-financing-micro-loan'>Working Capital Financing - Micro Loan</option><option value='working-capital-financing-p3-10'>Working Capital Financing - P3-10</option><option value='working-capital-financing-p3-20'>Working Capital Financing - P3-20</option><option value='working-capital-financing-p3-30'>Working Capital Financing - P3-30</option>");
				} else if (val == "capex") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='capex-loan'>Capex Loan</option>");
				} else if (val == "salary-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='institutional'>Institutional</option><option value='individual'>Individual</option><option value='barangay'>Barangay</option><option value='pensioner'>Pensioner</option><option value='deped'>DepEd</option><option value='pvao'>PVAO</option>");
				} else if (val == "professional-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='professional-loan-licensed-professional'>Professional Loan - Licensed Professional</option><option value='professional-loan-seaman'>Professional Loan - Seaman</option>");
				} else if (val == "ofw-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='ofw-beneficiary-loan'>OFW - Beneficiary Loan</option>");
				} else if (val == "personal-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='personal-loan-medical-services'>Personal Loan - Medical Services</option><option value='personal-loan-medical-services'>Personal Loan - Medical Services</option><option value='personal-loan-travel-vacation'>Personal Loan - Travel &amp; Vacation</option><option value='personal-loan-special-occasion'>Personal Loan - Special Occasion</option><option value='personal-loan-rfc-lessor-unsecured'>Personal Loan - RFC Lessor Unsecured</option><option value='personal-loan-rfc-lessor-secured'>Personal Loan - RFC Lessor Secured</option><option value='personal-loan-lessor-unsecured'>Personal Loan - Lessor Unsecured</option><option value='personal-loan-lessor-secured'>Personal Loan - Lessor Secured</option>");
				} else if (val == "education-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='educational-loan-secured'>Educational Loan - Secured</option><option value='educational-loan-secured-informatics'>Educational Loan - Secured Informatics</option><option value='educational-loan-unsecured'>Educational Loan - Unsecured</option><option value='educational-loan-unsecured-informatics'>Educational Loan - Unsecured Informatics</option>");
				} else if (val == "equity-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='equity-downpayment-loan'>Equity/Downpayment Loan</option>");
				} else if (val == "home-asset-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='vehicle-upgrade'>Vehicle Upgrade</option><option value='home-improvement'>Home Improvement</option><option value='home-construction'>Home Construction</option><option value=''></option>");
				} else if (val == "take-out-loan") {
					$("#sub-type").html("<option disabled='' selected=''></option><option value='take-out-loan'>Take Out Loan</option><option value='take-out-loan-credit-card-balance-transfer'>Take Out Loan - Credit Card Balance Transfer</option><option value='take-out-loan-loan-consolidation'>Take Out Loan - Loan Consolidation</option>");				
				}									
			});
            /*
			$("#sub-type").change(function () {
				var val = $(this).val();
				if (val == "appliance-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-150k'>10,000 - 150,000</option>");
				} else if (val == "appliance-loan-ofw") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='20k-150k'>20,000 - 150,000</option>");
				} else if (val == "personal-vehicle-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "motorcycle-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "motor-tricycle") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "taxi") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "jeepney") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "van") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "bus") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "commercial-boat") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "farming-machineries-equipment-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='1m-below'>1,000,000 Below</option><option value='1m-above'>1,000,000 Above</option>");
				} else if (val == "heavy-equipment-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "truck-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "medical-equipment-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='50k-2.5m'>50,000 - 2,500,000</option>");
				} else if (val == "specialized-equipment-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option>");
				} else if (val == "real-estate-housing-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "bridge-financing") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "receivable-check-discounting") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "inventory-financing-dealers-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-2.5m'>Upto 2,500,000</option>");
				} else if (val == "inventory-financing") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-100k'>Upto 100,000</option>");
				} else if (val == "working-capital-financing-franchise") {
					$( "#desired-loan" ).replaceWith( "<input type='text' name='desired-loan' class='form-control'>" );
				} else if (val == "working-capital-financing-wet-market") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='5k-20k'>5,000 - 20,000</option>");
				} else if (val == "working-capital-financing-unsecured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='20k'>20,000</option><option value='30k'>30,000</option><option value='40k'>40,000</option>");
				} else if (val == "working-capital-financing-secured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "working-capital-financing-micro-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option>");
				} else if (val == "working-capital-financing-p3-10") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='5k-10k'>5,000 - 10,000</option>");
				} else if (val == "working-capital-financing-p3-20") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-20k'>10,000 - 20,000</option>");
				} else if (val == "working-capital-financing-p3-30") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-30k'>10,000 - 30,000</option>");
				} else if (val == "capex-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "institutional") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option>");
				} else if (val == "individual") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option>");
				} else if (val == "barangay") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option>");
				} else if (val == "pensioner") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option>");
				} else if (val == "deped") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option>");
				} else if (val == "pvao") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option>");
				} else if (val == "professional-loan-licensed-professional") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='30k-100k'>30,000 - 100,000</option>");
				} else if (val == "professional-loan-seaman") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-30k'>10,000 - 30,000</option><option value='10k-50k'>10,000 - 50,000</option>");
				} else if (val == "ofw-beneficiary-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-30k'>10,000 - 30,000</option>");
				} else if (val == "personal-loan-medical-services") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-above'>251,000 Above</option>");
				} else if (val == "personal-loan-travel-&-vacation") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-above'>251,000 Above</option>");
				} else if (val == "personal-loan-special-occasion") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-above'>251,000 Above</option>");
				} else if (val == "personal-loan-rfc-lessor-unsecured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option>");
				} else if (val == "personal-loan-rfc-lessor-secured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option>");
				} else if (val == "personal-loan-lessor-unsecured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-500k'>251,000 - 500,000</option>");
				} else if (val == "personal-loan-lessor-secured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-500k'>251,000 - 500,000</option>");
				} else if (val == "educational-loan-secured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option>");
				} else if (val == "educational-loan-secured-informatics") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-50k'>41,000 - 50,000</option><option value='51k-80k'>51,000 - 80,000</option>");
				} else if (val == "educational-loan-unsecured") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-80k'>Upto 80,000</option>");
				} else if (val == "educational-loan-unsecured-informatics") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='upto-50k'>Upto 50,000</option><option value='50k-80k'>50,000 - 80,000</option>");
				} else if (val == "equity-downpayment-loan") {
					$( "#desired-loan" ).replaceWith( "<input type='text' name='desired-loan' class='form-control'>" );
				} else if (val == "vehicle-upgrade") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='20k-99k'>20,000 - 99,000</option><option value='100k-150k'>100,000 - 150,000</option>");
				} else if (val == "home-improvement") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "home-construction") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-80k'>10,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "take-out-loan") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "take-out-loan-credit-card") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				} else if (val == "take-out-loan-loan-consolidation") {
					$("#desired-loan").html("<option disabled='' selected=''></option><option value='10k-40k'>10,000 - 40,000</option><option value='41k-80k'>41,000 - 80,000</option><option value='81k-250k'>81,000 - 250,000</option><option value='251k-400k'>251,000 - 400,000</option><option value='401k-750k'>401,000 - 750,000</option><option value='751k-above'>751,000 Above</option>");
				}
			});
            */
		});
	</script>
</html>
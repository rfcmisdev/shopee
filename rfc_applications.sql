-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2018 at 04:05 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rfc360dbinstance`
--

-- --------------------------------------------------------

--
-- Table structure for table `rfc_applications`
--

CREATE TABLE IF NOT EXISTS `rfc_applications` (
  `mic_id` int(11) NOT NULL,
  `loan_type` varchar(100) NOT NULL,
  `loan_subtype` varchar(100) NOT NULL,
  `desired_loan` varchar(100) NOT NULL,
  `payment_term` varchar(100) NOT NULL,
  `loan_purpose` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `suffix` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `birthplace` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `landline` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `present_address` varchar(255) NOT NULL,
  `present_address_status` varchar(50) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `permanent_address_status` varchar(50) NOT NULL,
  `sss_no` varchar(100) NOT NULL,
  `tin_no` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `civil_status` varchar(100) NOT NULL,
  `education` varchar(100) NOT NULL,
  `employment_status` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `monthly_income` varchar(100) NOT NULL,
  `employment_year` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rfc_applications`
--

INSERT INTO `rfc_applications` (`mic_id`, `loan_type`, `loan_subtype`, `desired_loan`, `payment_term`, `loan_purpose`, `lastname`, `firstname`, `middlename`, `suffix`, `gender`, `birthday`, `birthplace`, `email_address`, `landline`, `mobile`, `present_address`, `present_address_status`, `permanent_address`, `permanent_address_status`, `sss_no`, `tin_no`, `nationality`, `civil_status`, `education`, `employment_status`, `company_name`, `position`, `monthly_income`, `employment_year`, `date_added`) VALUES
(1, 'appliance', 'appliance-loan', '50,000', 'monthly', 'testing', 'marimat', 'jhayar', 'jimeno', '', 'male', '0000-00-00', 'cavite', 'marimat@gmail.com', '', '+639286986591', 'mandaluyong', 'rented', 'cavite', 'owned', '12345', '123456', 'filipino', 'single', 'bsd', 'employed', 'rfc', 'soft dev', '10,000', 2, '2018-06-14 03:59:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rfc_applications`
--
ALTER TABLE `rfc_applications`
  ADD PRIMARY KEY (`mic_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rfc_applications`
--
ALTER TABLE `rfc_applications`
  MODIFY `mic_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

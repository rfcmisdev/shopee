<?php
	require ("db.php");
	error_reporting(0);
	set_time_limit(0);
	session_start();
	
	/*if (empty($_SESSION["USERCID"])){
		echo "Log In Required! Re-directing to Log In Screen...<script type='text/javascript' >window.setTimeout(function() { window.location = 'login.php'; }, 1200); </script>";
		exit();
	}*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Shopee Applications</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styleyourface.css">

  <!-- lib css -->
  <link rel="stylesheet" href="lib/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="lib/datatables/dataTables.bootstrap.min.css">
  <!-- lib js -->
  <script type="text/javascript" src="lib/jquery-1.12.3.js"></script>
  <script type="text/javascript" src="lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="lib/datatables/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="lib/datatables/dataTables.bootstrap.min.js"></script>

</head>

<script>

 $(document).ready(function(){
       var table = $('#branch_table').DataTable({
        "order": [[ 0, "desc" ]]
      });
    });

</script>

<body>
  <div style="background-color: #4a6e92; width: 100%;"><img src="rfc.png"></div>
  <div class="container" style="width: 100%; margin-top: 2%;">
  
  <div style="float:right"><a href="logout.php">LOGOUT</a></div>
  <div class='container' style="width: 100%;">  
    <form method="post" action="export.php">
      <input type="submit" name="export" value="Download CSV" class="button" />
      
    </form><br>
    <div class="scrollmenu">
    <table id="branch_table" class="table table-striped table-bordered" width="100%" cellspacing="0" >
      <thead >
        <tr height="27" >
          <div class="scrollmenu">
          <th>Loan ApplicationID</th>
          <th>Loan Purpose</th>
          <th>Loan Amount</th>
          <th>Payment Term</th>
          <th>First Name</th>
          <th>Middle Name</th>
          <th>Last Name</th>
          <th>Gender</th>
          <th>Marital Status</th>
          <th>Birthday</th>
          <th>Nationality</th>
          <th>Email Address</th>
          <th>City</th>
          <th>Barangay</th>
          <th>Street</th>
          <th>Years / Months of Stay</th>
          <th>Mobile No.</th>
          <th>Employment Type</th>
          <th>Employer Name</th>
          <th>Position</th>
          <th>Years / Months of Employment</th>
          <th>Salary</th>
          <th>Business Name</th>
          <th>Business Type</th>
          <th>Years / Months of Business</th>
          <th>Monthly Income</th>
          <th>Date Added</th>
          </div>
        </tr>
      </thead>

        <tbody style="font-size:11px; text-transform:capitalize">
          <?php
                $query = "SELECT sho_id,sho_loan_purpose,sho_loan_amount,sho_payment_term,
				sho_first_name,sho_middle_name,sho_last_name,sho_gender,sho_civil_status,sho_birthday,
				sho_nationality,sho_email,sho_city,sho_barangay,sho_street,sho_years_stay,sho_months_stay,
				sho_mobile,sho_employment_type,sho_employer_name,sho_company_position,sho_years_employment,sho_months_employment,
				sho_monthly_salary,sho_business_name,sho_business_type,sho_years_business,sho_months_business,
				sho_monthly_income,file_1,file_2,sho_timestamp
				FROM shopee_applications 
				WHERE 1 ORDER BY sho_id DESC";
                $result = mysqli_query($db, $query);
                  if ($result = $db->query($query)) {
                    while ($row = $result->fetch_array()) { 
                      list($sho_id,$sho_loan_purpose,$sho_loan_amount,$sho_payment_term,$sho_first_name,$sho_middle_name,$sho_last_name,$sho_gender,$sho_civil_status,$sho_birthday,$sho_nationality,$sho_email,$sho_city,$sho_barangay,$sho_street,$sho_years_stay,$sho_months_stay,$sho_mobile,$sho_employment_type,$sho_employer_name,$sho_company_position,$sho_years_employment,$sho_months_employment,$sho_monthly_salary,$sho_business_name,$sho_business_type,$sho_years_business,$sho_months_business,$sho_monthly_income,$file_1,$file_12,$sho_timestamp) = $row;
          			?>
					 <tr>
                      <td><?php echo $sho_id; ?></td>
                      <td><?php echo $sho_loan_purpose; ?></td>
                      <td><?php echo $sho_loan_amount; ?></td>
                      <td><?php echo $sho_payment_term; ?> Months</td>
                      <td><?php echo $sho_first_name; ?></td>
                      <td><?php echo $sho_middle_name; ?></td>
                      <td><?php echo $sho_last_name; ?></td>
                      <td><?php echo $sho_gender; ?></td>
                      <td><?php echo $sho_civil_status; ?></td>
                      <td><?php echo $sho_birthday; ?></td>
                      <td><?php echo $sho_nationality; ?></td>
                      <td><?php echo $sho_email; ?></td>
                      <td><?php echo $sho_city; ?></td>
                      <td><?php echo $sho_barangay; ?></td>
                      <td><?php echo $sho_street; ?></td>
                      <td><?php echo $sho_years_stay.' / '.$sho_months_stay.''; ?></td>
                      <td><?php echo $sho_mobile; ?></td>
                      <td><?php echo $sho_employment_type; ?></td>
                      <td><?php echo $sho_employer_name; ?></td>
                      <td><?php echo $sho_company_position; ?></td>
                      <td><?php echo $sho_years_employment.' / '.$sho_months_employment; ?></td>
                      <td><?php echo $sho_monthly_salary; ?></td>
                      <td><?php echo $sho_business_name; ?></td>
                      <td><?php echo $sho_business_type; ?></td>
                      <td><?php echo $sho_years_business.' / '.$sho_months_business; ?></td>
                      <td><?php echo $sho_monthly_income; ?></td>
                      <td><?php echo $sho_timestamp; ?></td>
        			</tr>
          			<?php
                  }
                }
          ?>
        </tbody>
    </table>
    <div>
  </div>
</body>
</html>
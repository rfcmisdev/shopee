<?php

	require 'vendor/autoload.php';
	include "db.php";
	
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;	

	// AWS Info
	$bucketName = 'newsite-external';
	$IAM_KEY = 'AKIAILVOCEFSDVVX4MQA';
	$IAM_SECRET = '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';

	// Connect to AWS
	try {
		// You may need to change the region. It will say in the URL when the bucket is open
		// and on creation.
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $IAM_KEY,
					'secret' => $IAM_SECRET
				),
				'version' => 'latest',
				'region'  => 'ap-southeast-1'
			)
		);
	} catch (Exception $e) {
		// We use a die, so if this fails. It stops here. Typically this is a REST call so this would
		// return a json object.
		die("Error: " . $e->getMessage());
	}


	//BEGIN
	$role = "7";
	$is_active = "0";
	$is_admin = "0";
	$username = mt_rand( 10000000, 99999999);
	$password_string = '!@#$%*&abcdefghijklmnpqrstuwxyzABCDEFGHJKLMNPQRSTUWXYZ23456789';
	$password = substr(str_shuffle($password_string), 0, 12);
	$ip = $_SERVER['HTTP_CLIENT_IP']?$_SERVER['HTTP_CLIENT_IP']:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']);
	$created_at = date('Y-m-d H:i:s');

	$first_name = $_POST["first-name"];
	$middle_name = $_POST["middle-name"];
	$last_name = $_POST["last-name"];
	$suffix_name = $_POST["suffix"];
	$marital_status = $_POST["marital-status"];
	$gender = $_POST["gender"]; 

	$birth_date_x = $_POST["dob_initial_month"].'/'.$_POST["dob_initial_day"].'/'.$_POST["dob_initial_year"];

	$date = DateTime::createFromFormat('m/d/Y', $birth_date_x);

	$birth_date = $date->format('d-F-Y');

	$age = (date('Y') - date('Y',strtotime($birth_date))); 

	$_POST["dob"] = $birth_date;
	$_POST["age"] = $age;

	$educational_attainment = $_POST["educational-attainment"];
	$nationality = $_POST["nationality"];
	$residency = $_POST["residency"];
	$province = $_POST["province"];
	$city = $_POST["city"];
	$barangay = $_POST["barangay"];
	$street_address = $_POST["street-address"];
	$years_stay = $_POST["years-of-stay"];
	$months_stay = $_POST["month-of-stay"];
	$email = $_POST["email-address"];
	//$landline = "02".$_POST["landline"];
	$mobile_no = "+63".$_POST["mobile-number"];
	$dependents_no = $_POST["number-of-dependents"];

	$spouse_first_name = $_POST["spouse-first-name"];
	$spouse_middle_name = $_POST["spouse-middle-name"];
	$spouse_last_name = $_POST["spouse-last-name"];
	$spouse_civil_status = $_POST["spouse-civil-status"];
	$spouse_gender = $_POST["spouse-gender"];

	if ($_POST["spouse_dob_initial_month"] != "") {
		$spouse_birth_date = $_POST["spouse_dob_initial_month"].'/'.$_POST["spouse_dob_initial_day"].'/'.$_POST["spouse_dob_initial_year"];
		$date_spouse = DateTime::createFromFormat('m/d/Y', $spouse_birth_date);
		$birth_date_spouse = $date_spouse->format('d-F-Y');
		$_POST["spouse-dob"] = $birth_date_spouse;
	} else {
		$_POST["spouse-dob"] = $_POST["spouse_dob_initial_month"].'/'.$_POST["spouse_dob_initial_day"].'/'.$_POST["spouse_dob_initial_year"]; 
	}

	$spouse_educational_attainment = $_POST["spouse-educational-attainment"];
	$spouse_employment_type = $_POST["spouse-employment-status"];
	$spouse_employer_name = $_POST["spouse-employer-name"];

	// 2nd Panel

	$employment_nature = $_POST["employment"];
	$employer_name = $_POST["employer-name"];
	$sector = $_POST["sector"];
	$position = $_POST["position"];
	//$profession = $_POST["profession"];
	$employment_years = $_POST["years-of-employment"];
	$employment_months = $_POST["month-of-employment"]; 
	$monthly_salary = intval(preg_replace('/[^\d.]/', '', $_POST["monthly-salary"]));
	//$non_core_business = $_POST["non-core-business"];

	$business_name = $_POST["business-name"];
	$business_type = $_POST["business-type"];
	$industry = $_POST["industry"];
	//$position = $_POST["position"];
	//$profession = $_POST["profession"];
	$business_years = $_POST["years-of-business"];
	$business_months = $_POST["month-of-business"];
	$monthly_income = intval(preg_replace('/[^\d.]/', '', $_POST["monthly-income"]));
	//$non_core_business = $_POST["non-core-business"];

	// 3rd Panel

	$loan_purpose = $_POST["loan-purpose"];
	$others = $_POST["other-loan-purpose"];
	$loan_product = $_POST["loan-product"];
	$sub_product = $_POST["sub-product"];
	$tenure_in_months = $_POST["tenure"];
	$repayment_frequency = $_POST["repayment-frequency"];
	$loan_amount_requested = intval(preg_replace('/[^\d.]/', '', $_POST["loan-amount-requested"]));
	$source_of_information = $_POST["source-of-information"];
	$additional_income = $_POST["additional-income"];
	$living_expense = intval(preg_replace('/[^\d.]/', '', $_POST["living-expense"]));
	$collateral_information = $_POST["collateral"];
	$collateral_type = $_POST["collateral-type"];
	$collateral_sub_type = $_POST["collateral-sub-type"];
	$collateral_category = $_POST["collateral-category"];
	$average_fair_market_value = intval(preg_replace('/[^\d.]/', '', $_POST["average-fair-market-value"]));

	if($_POST["monthly-salary"]) {
		$nc_var1 = intval(preg_replace('/[^\d.]/', '', $monthly_salary));
		$nc_var2 = intval(preg_replace('/[^\d.]/', '', $additional_income));
		$nc_var3 = intval(preg_replace('/[^\d.]/', '', $living_expense));

		$gross_income = $nc_var1 + $nc_var2;
		$expense = $nc_var3;
		$ndi = $gross_income - $expense;  
		$ndib = $ndi * 0.5; 
		$tex = $nc_var3;
		$exdef = $gross_income - $ndi;
	} else if ($_POST["monthly-income"]) {
		$nc_var1 = intval(preg_replace('/[^\d.]/', '', $monthly_income));
		$nc_var2 = intval(preg_replace('/[^\d.]/', '', $additional_income));
		$nc_var3 = intval(preg_replace('/[^\d.]/', '', $living_expense));

		$gross_income = $nc_var1 + $nc_var2;
		$expense = $nc_var3;
		$ndi = $gross_income - $expense; 
		$ndib = $ndi * 0.5; 
		$tex = $nc_var3;
		$exdef = $gross_income - $ndi;		
	}

	$keyNameS3 = $first_name. '_' .$last_name. '-' .$username. '/';

	// 4th Panel
	if(!$_FILES['primary_id']['name'] == "") {
	// For this, I would generate a unqiue random string for the key name. But you can do whatever.
		$filename = $_FILES["primary_id"]['name'];
		$file_basename = substr($filename, 0, strripos($filename, '.'));
		$file_ext = substr($filename, strripos($filename, '.'));
		$keyName = $first_name. '_' .$last_name. '-' .$username. '/' . '01-primary_id-'.$last_name. '_' . $first_name . $file_ext;
		$pathInS3 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName;

		// Add it to S3
		try {
			// Uploaded:
			$file = $_FILES["primary_id"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName,
					'SourceFile' => $file,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['secondary_id']['name'] == "") {
		$filename2 = $_FILES["secondary_id"]['name'];
		$file_basename2 = substr($filename2, 0, strripos($filename2, '.'));
		$file_ext2 = substr($filename2, strripos($filename2, '.'));
		$keyName2 = $first_name. '_' .$last_name. '-' .$username. '/' . '02-secondary_id-'.$last_name. '_' . $first_name . $file_ext2;
		$pathInS32 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName2;

		// Add it to S3
		try {
			// Uploaded:
			$file2 = $_FILES["secondary_id"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName2,
					'SourceFile' => $file2,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['billing']['name'] == "") {
		$filename3 = $_FILES["billing"]['name'];
		$file_basename3 = substr($filename3, 0, strripos($filename3, '.'));
		$file_ext3 = substr($filename3, strripos($filename3, '.'));
		$keyName3 = $first_name. '_' .$last_name. '-' .$username. '/' . '03-billing-'.$last_name. '_' . $first_name . $file_ext3;
		$pathInS33 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName3;

		// Add it to S3
		try {
			// Uploaded:
			$file3 = $_FILES["billing"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName3,
					'SourceFile' => $file3,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['payslip']['name'] == "") {
		$filename4 = $_FILES["payslip"]['name'];
		$file_basename4 = substr($filename4, 0, strripos($filename4, '.'));
		$file_ext4 = substr($filename4, strripos($filename4, '.'));
		$keyName4 = $first_name. '_' .$last_name. '-' .$username. '/' . '04-payslip-'.$last_name. '_' . $first_name . $file_ext4;		
		$pathInS34 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName4;

		// Add it to S3
		try {
			// Uploaded:
			$file4 = $_FILES["payslip"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName4,
					'SourceFile' => $file4,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['signed_consent']['name'] == "") {
		$filename5 = $_FILES["signed_consent"]['name'];
		$file_basename5 = substr($filename5, 0, strripos($filename5, '.'));
		$file_ext5 = substr($filename5, strripos($filename5, '.'));
		$keyName5 = $first_name. '_' .$last_name. '-' .$username. '/' . '05-signed_consent-'.$last_name. '_' . $first_name . $file_ext5;
		$pathInS35 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName5;

		// Add it to S3
		try {
			// Uploaded:
			$file5 = $_FILES["signed_consent"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName5,
					'SourceFile' => $file5,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}	
	}

	if(!$_FILES['certificate_employment']['name'] == "") {
		$filename6 = $_FILES["certificate_employment"]['name'];
		$file_basename6 = substr($filename6, 0, strripos($filename6, '.'));
		$file_ext6 = substr($filename6, strripos($filename6, '.'));
		$keyName6 = $first_name. '_' .$last_name. '-' .$username. '/' . '06-certificate_employment-'.$last_name. '_' . $first_name . $file_ext6;
		$pathInS36 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName6;

		// Add it to S3
		try {
			// Uploaded:
			$file6 = $_FILES["certificate_employment"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName6,
					'SourceFile' => $file6,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}	
	}

	if(!$_FILES['dti_mayors_permit']['name'] == "") {
		$filename7 = $_FILES["dti_mayors_permit"]['name'];
		$file_basename7 = substr($filename7, 0, strripos($filename7, '.'));
		$file_ext7 = substr($filename7, strripos($filename7, '.'));
		$keyName7 = $first_name. '_' .$last_name. '-' .$username. '/' . '07-dti_mayors_permit-'.$last_name. '_' . $first_name . $file_ext7;
		$pathInS37 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName7;

		// Add it to S3
		try {
			// Uploaded:
			$file7 = $_FILES["dti_mayors_permit"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName7,
					'SourceFile' => $file7,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}	
	}
	// END

	$query1 = "INSERT INTO ci_users_applicant SET 

		username = '".$username."', 
		firstname = '".$first_name."', 
		middlename = '".$middle_name."', 
		lastname = '".$last_name."', 
		suffix = '".$suffix."', 
		email = '".$email."', 
		mobile_no = '".$mobile_no."', 
		password = '".$password."', 
		role = '".$role."', 
		is_active = '".$is_active."', 
		is_admin = '".$is_admin."', 
		last_ip = '".$ip."', 
		created_at = '".$created_at."', 
		updated_at = '".$created_at."', 
		marital_status = '".$marital_status."', 
		gender = '".$gender."', 
		birthday = '".$birth_date."',
		age = '".$age."', 
		educational_attainment = '".$educational_attainment."', 
		nationality = '".$nationality."', 
		residency = '".$residency."', 
		province = '".$province."', 
		city = '".$city."',
		barangay = '".$barangay."', 
		street_address = '".$street_address."', 
		years_of_stay = '".$years_stay."', 
		months_of_stay = '".$months_stay."', 
		landline = '".$landline."', 
		number_of_dependents = '".$dependents_no."', 
		spouse_first_name = '".$spouse_first_name."', 
		spouse_middle_name = '".$spouse_middle_name."', 
		spouse_last_name = '".$spouse_last_name."', 	
		spouse_civil_status = '".$spouse_civil_status."', 
		spouse_gender = '".$spouse_gender."', 
		spouse_birthday = '".$birth_date_spouse."', 
		spouse_educational_attainment = '".$spouse_educational_attainment."', 
		spouse_employment_type = '".$spouse_employment_type."', 
		spouse_employer_name = '".$spouse_employer_name."', 
		employment_nature = '".$employment_nature."', 
		employer_name = '".$employer_name."', 
		sector = '".$sector."', 
		position = '".$position."', 
		profession = '".$profession."', 
		employment_years = '".$employment_years."', 
		employment_months = '".$employment_months."', 
		monthly_salary = '".$monthly_salary."', 
		non_core_business = '".$non_core_business."', 
		business_name = '".$business_name."', 
		business_type = '".$business_type."', 
		industry = '".$industry."', 
		business_years = '".$business_years."', 
		business_months = '".$business_months."', 
		monthly_income = '".$monthly_income."', 
		loan_purpose = '".$loan_purpose."', 
		other_loan_purpose = '".$others."', 
		loan_product = '".$loan_product."', 
		sub_product = '".$sub_product."', 
		tenure = '".$tenure_in_months."', 
		repayment_frequency = '".$repayment_frequency."', 
		loan_amount_requested = '".$loan_amount_requested."', 
		source_of_information = '".$source_of_information."', 
		additional_income = '".$additional_income."', 
		living_expense = '".$living_expense."', 
		collateral_information = '".$collateral_information."',
		collateral_type = '".$collateral_type."', 
		collateral_subtype = '".$collateral_sub_type."', 
		collateral_category = '".$collateral_category."', 
		average_fair_market_value = '".$average_fair_market_value."',
		gross_income = '".$gross_income."',
		net_disposable_income = '".$ndi."',
		net_disposable_income_buffer = '".$ndib."',
		total_expense = '".$tex."',
		excess_deficiency = '".$exdef."',
		key_name = '".$keyNameS3."',
		file_1 = '".$pathInS3."',
		file_2 = '".$pathInS32."',
		file_3 = '".$pathInS33."',
		file_4 = '".$pathInS34."',
		file_5 = '".$pathInS35."', 
		file_6 = '".$pathInS36."',
		file_7 = '".$pathInS37."'";  	
	
	if (mysqli_query($db, $query1)) {

		 // echo "Data Inserted";
		 file_put_contents('newsite_application.json', json_encode($_POST));
		 header("location:https://cst.rfc.com.ph/rfc-extern/public/api_screening_tool_prod.php");
		 
	} else {
		
		echo "Error: " . $query1 . "<br>" . mysqli_error($db);
	
	}

	$db->close();
?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Radiowealth Finance Corporation - Loan Application</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="icon" type="image/png" href="assets/img/rfc-favicon.png" />
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131206343-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-131206343-1');
        </script>

    </head>

    <body>
    
    <!--
    Start of Floodlight Tag: Please do not remove
    Activity name of this tag: Conversion Pixel
    URL of the webpage where the tag is expected to be placed: http://www.mediadonuts.com
    This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
    Creation Date: 12/19/2018
    -->
    <iframe src="https://8882714.fls.doubleclick.net/activityi;src=8882714;type=conve0;cat=conve0;qty=1;cost=[Revenue];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=[OrderID]?" width="1" height="1" frameborder="0" style="display:none"></iframe>
    <!-- End of Floodlight Tag: Please do not remove -->

        <div class="top-content">
                <div class="container">
                        
                <div class="col-xs-6 col-xs-offset-3 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 form-box">
                    <div class="f1">
                        <div class="row">
                            
                                <div class="col-sm-12">
                                        <h2 class="text-center">Thank you for signing up!</h2>
                                        <div class="col-sm-12">&nbsp;</div>
                                        <div class="col-sm-10 col-sm-offset-1">
										<h4 class="text-center">Thank you for filling out the forms. We really appreciate you giving us a moment of your time today.</h4>
                                        </div>
                                        <div class="col-sm-12">&nbsp;</div>                                
		                                    <div class="icon" style="color: #001e96; border-color: #001e96; font-size: 64px;">
		                                        <i class="fa fa-check"></i>
		                                    </div>
										<br> 
										<a href="https://www.rfc.com.ph/" class="btn btn-fill btn-primary text-uppercase" style="background-color: #001e96; margin: 0 auto; display: block; width: 200px;">Back To RFC Website</a>								
									</div>
                                    <div class="col-sm-12">&nbsp;</div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>

		<script type="text/javascript">
			history.pushState(null, null, location.href);	
    		window.onpopstate = function () {
        		history.go(1);
    		};
		</script>
    </body>
</html>
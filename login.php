<?php
   require("db.php");
   require("funcs.php");
	
    ob_start();
    session_start();
    if(isset($_POST['userid']) && isset($_POST['password'])){
        
        $userid = mysql_escape_mimic($_POST['userid']);
        $password = mysql_escape_mimic($_POST['password']);

        $sql = "select userid, usr_name, usr_pass, cstmapid from lendr_usrs where usr_name='$userid' and usr_pass=SHA2('$password',224)";
		if ($result = $db->query($sql)) {
			$row_count = $result->num_rows;
			while ($row = $result->fetch_array()) { 
				list ($userid, $usr_name, $usr_pass, $cstmapid) = $row;
			}
			
		}
		
		if($row_count > 0){
			$_SESSION["USERCID"]=$userid;
			$_SESSION["USERNAME"]=$usr_name;
			$_SESSION["USERCSTID"]=$cstmapid;
			header("location: ./admin.php");
		}
		else{
			echo "Check credentials!";
		}
	}
			
    
?>

<html>	
<head>
        <title>RFC System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- lib css -->
    <link rel="stylesheet" href="lib/bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/datatables/dataTables.bootstrap.min.css">
    <!-- lib js -->
    <script type="text/javascript" src="lib/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="lib/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="lib/datatables/dataTables.bootstrap.min.js"></script>
    
    <!-- other css -->
    <link rel="stylesheet" type="text/css" href="css/fontDeclaration.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">

</head>

<body>

    <div class="container" style="margin-top:140px">
        <?php if(isset($_GET['message'])){ ?>

        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $_GET['message']; ?>
        </div>

        <?php } ?>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img src="./lib/images/rfc_logo.jpg" height="70px" width="300px">
                       <!--  <h3 class="panel-title">
                        Login</h3> -->
                    </div>
                    <div class="panel-body">
                        <form role="form" id="myform" action="" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" id="userid" placeholder="User ID" name="userid" type="text" required="true" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="password" placeholder="Password" name="password" type="password" required="true">
                                </div>
                                <!-- <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div> -->
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block" name="Login">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
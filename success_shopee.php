<?php
	error_reporting(0);
	require 'vendor/autoload.php';
	include "db.php";
	
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;	

	// AWS Info
	$bucketName = 'newsite-external';
	$IAM_KEY = 'AKIAILVOCEFSDVVX4MQA';
	$IAM_SECRET = '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';

	// Connect to AWS
	try {
		// You may need to change the region. It will say in the URL when the bucket is open
		// and on creation.
		$s3 = S3Client::factory(
			array(
				'credentials' => array(
					'key' => $IAM_KEY,
					'secret' => $IAM_SECRET
				),
				'version' => 'latest',
				'region'  => 'ap-southeast-1'
			)
		);
	} catch (Exception $e) {
		// We use a die, so if this fails. It stops here. Typically this is a REST call so this would
		// return a json object.
		die("Error: " . $e->getMessage());
	}

	$username = mt_rand( 10000000, 99999999);
	$shopee_username = $_POST["shopee_username"];
	$loan_amount = $_POST["loan-amount"];
	$payment_term = $_POST["payment-term"];
	$loan_purpose = $_POST["loan-purpose"];
	$first_name = $_POST["first-name"];
	$middle_name = $_POST["middle-name"];
	$last_name = $_POST["last-name"];
	$gender = $_POST["gender"];
	//$dob_orig = $_POST["dob"];
	$dob = $_POST["dob_initial_year"].'-'.$_POST["dob_initial_month"].'-'.$_POST["dob_initial_day"];
	$civil_status = $_POST["marital-status"];
	$nationality = $_POST["nationality"];
	$email = $_POST["email-address"];
	$province = $_POST["province"];
	$city = $_POST["city"];
	$barangay = $_POST["barangay"];
	$street = $_POST["street-address"];
	$years_stay = $_POST["years-of-stay"];
	$months_stay = $_POST["month-of-stay"];
	$mobile = "63".$_POST["mobile-number"];
	$employment_type = $_POST["employment"];
	$employer_name = $_POST["employer-name"];
	$company_position = $_POST["position"];
	$years_employment = $_POST["years-of-employment"];
	$months_employment = $_POST["month-of-employment"];
	
	$monthly_salary = $_POST["monthly-salary"];
	$business_name = $_POST["business-name"];
	$business_type = $_POST["business-type"];
	$years_business = $_POST["years-of-business"];
	$months_business = $_POST["month-of-business"];
	$monthly_income = $_POST["monthly-income"];
	
	
	$user_id = '105';
	$branch_id = '3';
	$cst_status = 'draft_pre';
	$timestamp = date("Y-m-d H:i:s");
	$app_user_role = 'branch';
	$app_branch_id = '1';

	$keyNameS3 = $first_name. '_' .$last_name. '-' .$username. '/';
	
	if(!$_FILES['primary_id']['name'] == "") {
	// For this, I would generate a unqiue random string for the key name. But you can do whatever.
		$filename = $_FILES["primary_id"]['name'];
		$file_basename = substr($filename, 0, strripos($filename, '.'));
		$file_ext = substr($filename, strripos($filename, '.'));
		$keyName = $first_name. '_' .$last_name. '-' .$username. '/' . '01-primary_id-'.$last_name. '_' . $first_name . $file_ext;
		$pathInS3 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName;

		// Add it to S3
		try {
			// Uploaded:
			$file = $_FILES["primary_id"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName,
					'SourceFile' => $file,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['secondary_id']['name'] == "") {
		$filename2 = $_FILES["secondary_id"]['name'];
		$file_basename2 = substr($filename2, 0, strripos($filename2, '.'));
		$file_ext2 = substr($filename2, strripos($filename2, '.'));
		$keyName2 = $first_name. '_' .$last_name. '-' .$username. '/' . '02-secondary_id-'.$last_name. '_' . $first_name . $file_ext2;
		$pathInS32 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName2;

		// Add it to S3
		try {
			// Uploaded:
			$file2 = $_FILES["secondary_id"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName2,
					'SourceFile' => $file2,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['billing']['name'] == "") {
		$filename3 = $_FILES["billing"]['name'];
		$file_basename3 = substr($filename3, 0, strripos($filename3, '.'));
		$file_ext3 = substr($filename3, strripos($filename3, '.'));
		$keyName3 = $first_name. '_' .$last_name. '-' .$username. '/' . '03-billing-'.$last_name. '_' . $first_name . $file_ext3;
		$pathInS33 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName3;

		// Add it to S3
		try {
			// Uploaded:
			$file3 = $_FILES["billing"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName3,
					'SourceFile' => $file3,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	if(!$_FILES['payslip']['name'] == "") {
		$filename4 = $_FILES["payslip"]['name'];
		$file_basename4 = substr($filename4, 0, strripos($filename4, '.'));
		$file_ext4 = substr($filename4, strripos($filename4, '.'));
		$keyName4 = $first_name. '_' .$last_name. '-' .$username. '/' . '04-payslip-'.$last_name. '_' . $first_name . $file_ext4;		
		$pathInS34 = 'https://s3.ap-southeast-1.amazonaws.com/' . $bucketName . '/' . $keyName4;

		// Add it to S3
		try {
			// Uploaded:
			$file4 = $_FILES["payslip"]['tmp_name'];
			$s3->putObject(
				array(
					'Bucket'=>$bucketName,
					'Key' =>  $keyName4,
					'SourceFile' => $file4,
					'StorageClass' => 'REDUCED_REDUNDANCY'
				)
			);
		} catch (S3Exception $e) {
			die('Error:' . $e->getMessage());
		} catch (Exception $e) {
			die('Error:' . $e->getMessage());
		}
	}

	####################### SAVE TO RFC APPLICATIONS #########################
		$query = "INSERT INTO shopee_applications(sho_first_name,sho_middle_name,sho_last_name,sho_gender,
			sho_birthday,sho_civil_status,sho_nationality,sho_email,sho_province, sho_city,sho_barangay,sho_street,
			sho_years_stay,sho_months_stay,sho_mobile,sho_employment_type,sho_employer_name,sho_company_position,
			sho_years_employment,sho_months_employment,sho_monthly_salary,sho_monthly_income,sho_business_name,
			sho_business_type,sho_years_business,sho_months_business,sho_loan_amount,sho_payment_term,sho_loan_purpose,sho_timestamp,
			keyNameS3,file_1,file_2,file_3,file_4,shopee_username)
		VALUES('$first_name','$middle_name','$last_name','$gender',
			'$dob','$civil_status','$nationality','$email','$province','$city','$barangay','$street',
			'$years_stay','$months_stay','$mobile','$employment_type','$employer_name','$company_position',
			'$years_employment','$months_employment','$monthly_salary','$monthly_income','$business_name',
			'$business_type','$years_business','$months_business','$loan_amount','$payment_term','$loan_purpose','$timestamp',
			'$keyNameS3','$pathInS3','$pathInS32','$pathInS33','$pathInS34','$shopee_username'
			)";
		$insert = $db->query($query);
		if($insert){
			//echo "Save ";
		}	

?>

<!doctype html>
<html lang="en">
	<head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Radiowealth Finance Corporation - Loan Application</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="icon" type="image/png" href="assets/img/rfc-favicon.png" />
</head>

<body>
	<div class="image-container set-full-height" style="">

	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">
		            <!--      Wizard container        -->
		            <div class="wizard-container" style="margin-top:25%">
						<!-- Landing section -->
						<div class="card wizard-card" data-color="blue" id="landing">
							<div class="wizard-header">
								<h3 class="wizard-title" style="color: #001e96;">Thank you for signing up!!</h3>								
							</div>
							<div class="tab-content">
								<div class="row">
									<div class="col-sm-12">
										<h5 class="text-center">Please wait for the call of our loan provider!</h5>
										<div class="choice" data-toggle="wizard-checkbox">		                                
		                                    
		                                </div>
										<br> 
																	
									</div>
								</div>
							</div>
						</div>

		            </div> <!-- wizard container -->
		        </div>
	        </div> <!-- row -->
	    </div> <!--  big container -->

	   <div class="footer">
	        <div class="container text-center">
	        </div>
	    </div>
	</div>

</body>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>

</html>

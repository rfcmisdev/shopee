<?php
session_start();
if (empty($_SESSION["USERCID"])){
	header ("location: login.php");
	exit();
}
	
require ("db.php");
if(isset($_POST["export"]))
{
	$date_today = date("Ymdhis");
	$filename = "shopee_".$date_today.".csv";
	header('Content-Type: text/csv; charset=utf-8');
	header("Content-Disposition: attachment; filename=$filename");
	$output = fopen("php://output", "w");
	fputcsv($output, array('Shopee Username', 'Loan Purpose', 'Loan Amount', 'Payment Term', 'First Name', 'Middle Name', 'Last Name', 'Gender', 'Marital Status', 'Birthday', 'Nationality', 'Email Address', 'Province', 'City', 'Barangay', 'Street', 'Years of Stay', 'Months of Stay','Mobile No', 'Employment Type', 'Employer Name', 'Position', 'Years of Employment', 'Months of Employment','Salary', 'Business Name', 'Business Type', 'Years of Business', 'Months of Business','Monthly Income', 'Date Added'));

		$query = "
		SELECT  shopee_username,sho_loan_purpose,sho_loan_amount,sho_payment_term,
				sho_first_name,sho_middle_name,sho_last_name,sho_gender,sho_civil_status,sho_birthday,
				sho_nationality,sho_email,sho_province, sho_city,sho_barangay,sho_street,sho_years_stay,sho_months_stay,
				sho_mobile,sho_employment_type,sho_employer_name,sho_company_position,sho_years_employment,sho_months_employment,
				sho_monthly_salary,sho_business_name,sho_business_type,sho_years_business,sho_months_business,
				sho_monthly_income,sho_timestamp 
		FROM shopee_applications 
		WHERE shopee_username!='' ORDER BY sho_id DESC";
		$result = mysqli_query($db, $query);
		while($row = mysqli_fetch_assoc($result)) {
			fputcsv($output, $row);
		}
	fclose($output);
}
?>
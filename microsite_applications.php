<?php
	require ("connmysql.php");
	error_reporting(0);
	set_time_limit(0);
	session_start();
	
	if (empty($_SESSION["USERCID"])){
		echo "Log In Required! Re-directing to Log In Screen...<script type='text/javascript' >window.setTimeout(function() { window.location = 'login.php'; }, 1200); </script>";
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Microsite</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/styleyourface.css">

  <!-- lib css -->
  <link rel="stylesheet" href="lib/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="lib/datatables/dataTables.bootstrap.min.css">
  <!-- lib js -->
  <script type="text/javascript" src="lib/jquery-1.12.3.js"></script>
  <script type="text/javascript" src="lib/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="lib/datatables/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="lib/datatables/dataTables.bootstrap.min.js"></script>

</head>

<script>

 $(document).ready(function(){
       var table = $('#branch_table').DataTable({
        "order": [[ 0, "desc" ]]
      });
    });

</script>

<body>
  <div style="background-color: #4a6e92; width: 100%;"><img src="rfc.png"></div>
  <div class="container" style="width: 100%; margin-top: 2%;">
  
  <div style="float:right"><a href="logout.php">LOGOUT</a></div>
  <div class='container' style="width: 100%;">  
    <form method="post" action="export.php">
      <input type="submit" name="export" value="Download CSV" class="button" />
      
    </form><br>
    <div class="scrollmenu">
    <table id="branch_table" class="table table-striped table-bordered" width="100%" cellspacing="0" >
      <thead >
        <tr height="27" >
          <div class="scrollmenu">
          <th>CST ApplicationID</th>
          <th>Loan Type</th>
          <th>Loan Subtype</th>
          <th>Desired Loan</th>
          <th>Payment Term</th>
          <th>Loan Purpose</th>
          <th>Last Name</th>
          <th>First Name</th>
          <th>Middle Name</th>
          <th>Suffix</th>
          <th>Gender</th>
          <th>Birthday</th>
          <th>Birthplace</th>
          <th>Email Address</th>
          <th>Landline</th>
          <th>Mobile</th>
          <th>Present Address</th>
          <th>Present Address Status</th>
          <th>Permanent Address</th>
          <th>Permanent Address Status</th>
          <th>SSS No.</th>
          <th>TIN No.</th>
          <th>Nationality</th>
          <th>Civil Status</th>
          <th>Education</th>
          <th>Employment Status</th>
          <th>Company Name</th>
          <th>Position</th>
          <th>Monthly Income</th>
          <th>Employment Year</th>
          <th>Date Added</th>
          </div>
        </tr>
      </thead>

        <tbody style="font-size:11px; text-transform:capitalize">
          <?php
                $query = "SELECT cst_applicationid, 
				loan_type, loan_subtype, desired_loan, payment_term, loan_purpose, lastname, firstname, middlename, 
				suffix, gender, birthday, birthplace, email_address, landline, mobile, present_address, 
				present_address_status, permanent_address, permanent_address_status, sss_no, tin_no, nationality, 
				civil_status, education, employment_status, company_name, position, monthly_income, employment_year, date_added
				FROM rfc_applications 
				WHERE cst_applicationid!=0 ORDER BY mic_id DESC";
                $result = mysqli_query($conn, $query);
                  if ($result = $conn->query($query)) {
                    while ($row = $result->fetch_array()) { 
                      list($cst_applicationid, $loan_type, $loan_subtype, $desired_loan, $payment_term, $loan_purpose, $lastname, $firstname, $middlename, $suffix, $gender, $birthday, $birthplace, $email_address, $landline, $mobile, $present_address, $present_address_status, $permanent_address, $permanent_address_status, $sss_no, $tin_no, $nationality, $civil_status, $education, $employment_status, $company_name, $position, $monthly_income, $employment_year, $date_added) = $row;
          ?>

        <tr>
          <td><?php echo $cst_applicationid; ?></td>
          <td><?php echo $loan_type; ?></td>
          <td><?php echo $loan_subtype; ?></td>
          <td><?php echo number_format($desired_loan,2); ?></td>
          <td><?php echo $payment_term; ?></td>
          <td><?php echo $loan_purpose; ?></td>
          <td><?php echo $lastname; ?></td>
          <td><?php echo $firstname; ?></td>
          <td><?php echo $middlename; ?></td>
          <td><?php echo $suffix; ?></td>
          <td><?php echo $gender; ?></td>
          <td><?php echo $birthday; ?></td>
          <td><?php echo $birthplace; ?></td>
          <td><?php echo $email_address; ?></td>
          <td><?php echo $landline; ?></td>
          <td><?php echo $mobile; ?></td>
          <td><?php echo $present_address; ?></td>
          <td><?php echo $present_address_status; ?></td>
          <td><?php echo $permanent_address; ?></td>
          <td><?php echo $permanent_address_status; ?></td>
          <td><?php echo $sss_no; ?></td>
          <td><?php echo $tin_no; ?></td>
          <td><?php echo $nationality; ?></td>
          <td><?php echo $civil_status; ?></td>
          <td><?php echo $education; ?></td>
          <td><?php echo $employment_status; ?></td>
          <td><?php echo $company_name; ?></td>
          <td><?php echo $position; ?></td>
          <td><?php echo number_format(trim($monthly_income),2); ?></td>
          <td><?php echo $employment_year; ?></td>
          <td><?php echo $date_added; ?></td>

        </tr>
          <?php
                  }
                }
          ?>
        </tbody>
    </table>
    <div>
  </div>
</body>
</html>
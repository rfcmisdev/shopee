<?php
	class Applicant_model extends CI_Model{

		public function add_user($data){
			$this->db->insert('ci_users_applicant', $data);
			return true;
		}

		public function get_all_users(){
			$this->db->where('is_admin', 0);
			$query = $this->db->get('ci_users_applicant');
			return $result = $query->result_array();
		}
		public function get_all_users_for_csv(){
			$this->db->where('is_admin', 0);
			$this->db->select('id, username, firstname, lastname, email, mobile_no, created_at');
			$this->db->from('ci_users_applicant');
			$query = $this->db->get();
			return $result = $query->result_array();
		}
		

		public function get_user_by_id($id){
			$query = $this->db->get_where('ci_users_applicant', array('id' => $id));
			return $result = $query->row_array();
		}

		public function edit_user($data, $id){
			$this->db->where('id', $id);
			return true;
		}

	}

?>
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Applicants extends MY_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/applicant_model', 'applicant_model');
		}

		public function index(){
			$data['all_users'] =  $this->applicant_model->get_all_users();
			$data['title'] = 'Applicant List';
			$data['view'] = 'admin/applicants/applicant_list';
			$this->load->view('admin/layout', $data);
		}
		
		//---------------------------------------------------------------
		//  Edit User
		public function edit($id = 0){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('username', 'Username', 'trim|required');
				$this->form_validation->set_rules('firstname', 'Username', 'trim|required');
				$this->form_validation->set_rules('lastname', 'Lastname', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|required');
				$this->form_validation->set_rules('mobile_no', 'Number', 'trim|required');
				$this->form_validation->set_rules('status', 'Status', 'trim|required');
				$this->form_validation->set_rules('group', 'Group', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$data['user'] = $this->applicant_model->get_user_by_id($id);
					$data['title'] = 'Edit User';
					$data['view'] = 'admin/applicants/applicant_edit';
					$this->load->view('admin/layout', $data);
				}
				else{
					$data = array(
						'username' => $this->input->post('username'),
						'firstname' => $this->input->post('firstname'),
						'lastname' => $this->input->post('lastname'),
						'email' => $this->input->post('email'),
						'mobile_no' => $this->input->post('mobile_no'),
						'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
						'role' => $this->input->post('group'),
						'is_active' => $this->input->post('status'),
						'updated_at' => date('Y-m-d : h:m:s'),
					);
					$data = $this->security->xss_clean($data);
					$result = $this->user_model->edit_user($data, $id);
					if($result){
						$this->session->set_flashdata('msg', 'User has been Updated Successfully!');
						redirect(base_url('admin/applicants'));
					}
				}
			}
			else{
				$data['user'] = $this->applicant_model->get_user_by_id($id);
				$data['title'] = 'Edit User';
				$data['view'] = 'admin/applicants/applicant_edit';
				$this->load->view('admin/layout', $data);
			}
		}

		//---------------------------------------------------------------
		//  Delete Users
		public function del($id = 0){
			$this->db->delete('ci_users_applicant', array('id' => $id));
			$this->session->set_flashdata('msg', 'User has been Deleted Successfully!');
			redirect(base_url('admin/applicants'));
		}

		//---------------------------------------------------------------
		//  Export Users PDF 
		public function create_users_pdf(){
			$this->load->helper('pdf_helper'); // loaded pdf helper
			$data['all_users'] = $this->applicant_model->get_all_users();
			$this->load->view('admin/applicants/users_pdf', $data);
		}

		//---------------------------------------------------------------	
		// Export data in CSV format 
		public function export_csv(){ 
		   // file name 
		   $filename = 'applicants_'.date('Y-m-d').'.csv'; 
		   header("Content-Description: File Transfer"); 
		   header("Content-Disposition: attachment; filename=$filename"); 
		   header("Content-Type: application/csv; ");
		   
		   // get data 
		   $user_data = $this->applicant_model->get_all_users_for_csv();

		   // file creation 
		   $file = fopen('php://output', 'w');
		 
		   $header = array("ID", "Username", "First Name", "Last Name", "Email", "Mobile_no", "Created Date"); 
		   fputcsv($file, $header);
		   foreach ($user_data as $key=>$line){ 
		     fputcsv($file,$line); 
		   }
		   fclose($file); 
		   exit; 
		  }
	}


?>
<?php
	error_reporting(0);
	include "db.php";

	$loan_type = $_POST["loan-type"];
	$sub_type = $_POST["sub-type"];
	$loan = $_POST["desired-loan"];
	$loan = str_replace(',','',$loan);
	$term = $_POST["payment-term"];
	$purpose = $_POST["purpose"];
	
	$last_name = $_POST["last-name"];
	$first_name = $_POST["first-name"];
	$middle_name = $_POST["middle-name"];
	$suffix = $_POST["suffix"];
	$gender = $_POST["gender"];
	$dob_orig = $_POST["dob"];
	$dob = date("Y-m-d", strtotime($dob_orig));
	$pob = $_POST["place-of-birth"];
	$email = $_POST["email"];
	
	$phone = $_POST["phone"];
	$mobile = $_POST["mobile"];
	$present_add = $_POST["present-address"];

	if (isset($_POST["ps-rented"])) {
		$ps_type = $_POST["ps-rented"];
	} elseif (isset($_POST["ps-owned"])) {
		$ps_type = $_POST["ps-owned"];
	} elseif (isset($_POST["ps-owned-mortgaged"])) {
		$ps_type = $_POST["ps-owned-mortgaged"];
	} elseif (isset($_POST["ps-with-relatives-parents"])) {
		$ps_type = $_POST["ps-with-relatives-parents"];
	} else {
		$ps_type = $_POST["ps-rented"];
	}

	$permanent_add = $_POST["permanent-address"];

	if (isset($_POST["pm-rented"])) {
		$pm_type = $_POST["pm-rented"];
	} elseif (isset($_POST["pm-owned"])) {
		$pm_type = $_POST["pm-owned"];
	} elseif (isset($_POST["pm-owned-mortgaged"])) {
		$pm_type = $_POST["pm-owned-mortgaged"];
	} elseif (isset($_POST["pm-with-relatives-parents"])) {
		$pm_type = $_POST["pm-with-relatives-parents"];
	} else {
		$pm_type = $_POST["pm-rented"];
	}

	$sss = $_POST["sss-gsis"];
	$tin = $_POST["tin"];
	$nationality = $_POST["nationality"];
	$civil_status = $_POST["civil-status"];
	$education = $_POST["education"];
	
	$spouse_last_name = $_POST["spouse-last-name"];
	$spouse_first_name = $_POST["spouse-first-name"];
	$spouse_middle_name = $_POST["spouse-middle-name"];
	$spouse_suffix = $_POST["spouse-suffix"];
	$spouse_contact = $_POST["spouse-phone"];
	
	$emp_status = $_POST["employment-status"];
	$com_name = $_POST["company-name"];
	$com_position = $_POST["company-position"];
	$mon_income = $_POST["monthly-income"];
	$mon_income = str_replace(',','',$mon_income);
	$emp_years = $_POST["years-employment"];
	$date_added = date("Y-m-d h:i:s");
	
	$user_id = '105';
	$branch_id = '3';
	$cst_status = 'draft_pre';
	$cst_date_created = date("Y-m-d");
	$app_user_role = 'branch';
	$app_branch_id = '1';
	
		
	####################### SAVE TO RFC APPLICATIONS #########################
	if(isset($_POST['submit'])){
		$sql_insertCST = "INSERT into applications(user_id, app_email,branch_id,nationality,marital_status,street_address,
				    landline,mobile_number,education_level,
					employer, position_as_employee, year_as_employee,monthly_income, 
					product, subproduct,loan_amount_requested, employment_status,
					repayment_frequency, loan_purpose, status, date_created, created_at,
					first_name, middle_name, last_name, suffix, date_of_birth, gender,
					app_user_role, app_branch_id, created_by_id)
				    VALUES('$user_id', '$email','$branch_id','$nationality','$civil_status','$permanent_add',
				    '$phone','$mobile','$education',
					'$com_name', '$com_position', '$emp_years','$mon_income', 
					'$loan_type', '$sub_type','$loan','$emp_status',
					'$term', '$purpose', '$cst_status', '$cst_date_created', '$date_added',
					'$first_name', '$middle_name', '$last_name', '$suffix', '$dob', '$gender',
					'$app_user_role', '$app_branch_id', '$user_id')";
		if ($db->query($sql_insertCST)) {
			$cst_applicationid = $db->insert_id;
			
			$query = "INSERT INTO rfc_applications(cst_applicationid, loan_type,loan_subtype,desired_loan,payment_term,loan_purpose,
				 lastname,firstname,middlename,suffix,gender,birthday,birthplace,email_address,
				 landline,mobile,present_address,present_address_status,permanent_address,permanent_address_status,
				 sss_no,tin_no,nationality,civil_status,education,
				 employment_status,company_name,position,monthly_income,employment_year,date_added)
			VALUES('$cst_applicationid','$loan_type','$sub_type','$loan','$term','$purpose',
					'$last_name','$first_name','$middle_name','$suffix','$gender','$dob','$pob','$email',
					'$phone','$mobile','$present_add','$ps_type','$permanent_add','$pm_type',
					'$sss','$tin','$nationality','$civil_status','$education',
					'$emp_status','$com_name','$com_position','$mon_income','$emp_years','$date_added')";
			$insert = $db->query($query);
			if($insert){
			}
		}	
	}

	

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Thank You | Radiowealth Finance Corporation</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/wizard.css" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="assets/css/styles.css" rel="stylesheet" />
</head>

<body>
	<div class="image-container set-full-height" style="background-image: url('assets/img/rfc-bg-image.jpg')">

	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
						<!-- Landing section -->
						<div class="card wizard-card" data-color="blue" id="landing">
							<div class="wizard-header">
								<h3 class="wizard-title" style="color: #001e96;">Thank you for signing up!</h3>								
							</div>
							<div class="tab-content">
								<div class="row">
									<div class="col-sm-12">
										<h5 class="text-center">Thank you for filling out the forms. We really appreciate<br>you giving us a moment of your time today.</h5>
										<div class="choice" data-toggle="wizard-checkbox">		                                
		                                    <div class="icon" style="color: #001e96; border-color: #001e96;">
		                                        <i class="fa fa-check"></i>
		                                    </div>
		                                </div>
										<br> 
										<a href="http://rfc.com.ph/" class="btn btn-fill btn-primary text-uppercase" style="background-color: #001e96; margin: 0 auto; display: block; width: 200px;">Back To RFC Website</a>								
									</div>
								</div>
							</div>
						</div>

		            </div> <!-- wizard container -->
		        </div>
	        </div> <!-- row -->
	    </div> <!--  big container -->

	   <div class="footer">
	        <div class="container text-center">
	        <p><a href="#" target="_blank">Terms and Condition</a> | <a href="#" target="_blank">Privacy Policy</a></p>
	        </div>
	    </div>
	</div>

</body>
	<!--   Core JS Files   -->
	<script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/wizard.js"></script>

	<!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>

</html>

<?php

	include "db.php";
	error_reporting(0);



?>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Radiowealth Finance Corporation - Loan Application</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css"> 

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="icon" type="image/png" href="https://loans.rfc.com.ph/assets/img/favicon.png" />
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131206343-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-131206343-1');
        </script>

    </head>

    <body>
    	
		<div class="top-content">
                <div class="container">
                        
                <div class="col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 form-box">
                    <div class="f1">
                        <div class="row">
                            
                                <div class="col-sm-12"  style="margin-top:25%">
                                        <h2 class="text-center">Thank you for signing up!</h2>
                                        <div class="col-sm-12">&nbsp;</div>
                                        <div class="col-sm-10 col-sm-offset-1">
										<h4 class="text-center">Please wait for the call of our loan provider!</h4>
                                        </div>
                                        <div class="col-sm-12">&nbsp;</div>                                
		                                    <div class="icon" style="color: #001e96; border-color: #001e96; font-size: 64px;">
		                                        <i class="fa fa-check"></i>
		                                    </div>
										<br> 
									
									</div>
                                    <div class="col-sm-12">&nbsp;</div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>

        <script type="text/javascript">
			history.pushState(null, null, location.href);	
    		window.onpopstate = function () {
        		history.go(1);
    		};
		</script>
    </body>
</html>
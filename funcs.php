<?php
	error_reporting(0);
	session_start();

	function site_security () {
		if (empty($_SESSION["USERCID"])) :
			echo "Log In Required! Re-directing to Log In Screen...<script type='text/javascript' >window.setTimeout(function() { window.location = 'index.php'; }, 1200); </script>";
			exit;
		endif;
	}

	function mysql_escape_mimic($inp){
	if(is_array($inp))
		return array_map(__METHOD__, $inp);
	if(!empty($inp) && is_string($inp)){
		return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
	}
	return $inp;
	}

	function session_destroyer(){
		session_start();
		session_unset();
		session_destroy();
	}

	function super_strip($inp) {
		$inp = trim($inp);
		$inp = stripslashes($inp);
		$inp = htmlspecialchars($inp);
		return $inp;
	}

?>

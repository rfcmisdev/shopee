<header class="l-header pos_fixed bg_solid shadow_wide" itemscope="" itemtype="https://schema.org/Header">
   <div class="l-subheader at_middle">
      <div class="l-subheader-h">
         <div class="l-subheader-cell at_left">
            <div class="w-img ush_image_1  with_transparent">
                <a class="w-img-h" href="/">
                    <img src="assets/img/easyrfc_logo.png" alt="">
                </a>
            </div>
         </div>
         <div class="l-subheader-cell at_center"></div>
         <div class="l-subheader-cell at_right">
            <div class="w-socials hidden_for_mobiles ush_socials_1  style_default hover_fade color_text shape_square">
               <!-- <a class="w-btn ush_btn_1 with_text_color  style_solid color_custom icon_none" href="https://www.rfc.com.ph/">
                    <span class="w-btn-label">BACK TO RFC</span>
                </a>-->	
            </div>
         </div>
      </div>
   </div>
   <div class="l-subheader for_hidden hidden"></div>
</header>
<style>
header.pos_fixed {
    position: fixed;
    left: 0;
    z-index: 111;
    width: 100%;    
}
.l-subheader.at_middle {
    box-shadow: 0 3px 5px -1px rgba(0,0,0,0.1), 0 2px 1px -1px rgba(0,0,0,0.05);
}
.header_hor .l-header.pos_fixed:not(.notransition) .l-subheader {
    transition-property: transform, background-color, box-shadow, line-height, height;
    transition-duration: 0.3s;
}
@media (min-width: 901px) {
    .l-subheader.at_middle {
        line-height: 100px;
        height: 100px;
    }
    .ush_image_1 {
        height: 70px;
    }    
    .ush_socials_1 {
        font-size: 18px;
    }    
    .w-btn.ush_btn_1 {
        font-size: 13px;
    }    
}
.l-subheader.at_middle {
    background-color: #ffffff;
    color: #4e4e4e;
    margin: 0 auto;
}
.l-subheader {
    padding-left: 2.5rem;
    padding-right: 2.5rem; 
}
.l-subheader-h {
    max-width: 1300px;
    display: flex;
    align-items: center;
    position: relative;
    margin: 0 auto;
    height: inherit;  
}
.l-subheader-cell {
    display: flex;
    align-items: center;
    flex-grow: 1;
    justify-content: flex-start;
}
.l-subheader-cell.at_left>* {
    margin-right: 1.4rem;
}
.w-img {
    flex-shrink: 0;
    transition: height 0.3s;
}
.l-header a {
    color: inherit;
}
.w-img-h {
    display: block;
    height: inherit;
    overflow: hidden;
}
.w-img img {
    display: block;
    height: inherit;
    width: auto;
}
img {
    height: auto;
    max-width: 100%;
}
.l-subheader-cell.at_right {
    justify-content: flex-end;
}
.l-subheader-cell {
    display: flex;
    align-items: center;
    flex-grow: 1;
}
.l-header .w-socials {
    line-height: 2em;
}
.l-subheader-cell.at_right>* {
    margin-left: 1.4rem;
}
.w-btn.ush_btn_1.style_solid {
    background-color: #ffc80a;
}
.w-btn.ush_btn_1 {
    border-color: #ffc80a;
    color: #ffffff;
}
.w-btn {
    display: inline-block;
    vertical-align: top;
    text-align: center;
    white-space: nowrap;
    position: relative;
    background: none;
    border: none;
    z-index: 0;
    cursor: pointer;
    transition: background-color 0.3s, border-color 0.3s, box-shadow 0.3s, opacity 0.3s, color 0.3s;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 400;
    line-height: 2.5;
    padding: 0 1.5em;
    border-radius: 1.5em;
    letter-spacing: 0.06em;
    box-shadow: 0 0em 0em rgba(0,0,0,0.18);
}
.w-btn.ush_btn_1:before, .w-btn.ush_btn_1.style_outlined:hover {
    background-color: #001e96;
}
.w-btn:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    height: 0;
    transition: height 0.3s;
    background-color: rgba(0,0,0,0.15);
}
.w-btn-label {
    position: relative;
}
.btn_hov_slide .w-btn.style_solid:after {
    border: none;
}
.w-btn:after {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    border-radius: inherit;
    border-width: 2px;
    border-style: solid;
    border-color: inherit;
}

@media (max-width: 900px){
    .ush_image_1{
        height: 60px;
        margin: 20px 0;
    }
}

@media (max-width: 600px){
    .top-content .description{
        margin: 140px 0 5px 0;
    }
}
</style>
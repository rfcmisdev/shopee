<?php 

    require_once ('db.php');

    $email = $_GET["email"];
    $vid = $_GET["vid"];

    $select = "SELECT * FROM ci_registered WHERE validation_id = '$vid'";
    $result = $db->query($select);

    if (isset($email) && isset($vid) && $result->num_rows > 0) {
        // Do Nothing
    } else {
?>
    <style>
        #overlay {
            position: fixed;
            display: block;
            width: 100%;
            height: 100%;
            top: 0; 
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255,255,255,1);
            z-index: 2;
            cursor: pointer;
        }    
        .form-post {
            position: absolute;
            top: 55%;
            left: 50%;
            font-size: 50px;
            user-select: none;
            transform: translate(-50%,-50%);
            -ms-transform: translate(-50%,-50%);
            width: 100%;
        }
        .form-post h2{
            font-size: 26px;
            color: #24242A;
        }
        .form-post h3{
            font-size: 20px;
            color: #4e4e4e;
        }
        .form-post p{
            font-size: 16px;
            color: #4e4e4e;
        }      
        .form-post input {
            width: 100%;
        }
        .f1-buttons-custom {
            text-align: center;
            margin-top: 2rem;
        }
        .success {
            display: none;
            max-width: 500px;
            position: relative;
            margin: 9em auto 0;            
        }
    </style>
    <div id="overlay" class="overlay">
        <form id="form-post" class="form-post f1" name="form-post">
            <div id="welcome" class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <!--<img class="for_default" src="/assets/img/RFC-logo-image.png" width="92" height="87" alt="">-->
                </div>
                <div class="col-sm-12">
                    <h2><strong>Hello! Welcome to RFC.</strong></h2>
                    <p>Before applying for a loan, we need to confirm that we got your email right.</p>
                    <p>Please verify your email address</p>      
                </div>
                <div class="col-sm-4 col-sm-offset-4">
                <label class="sr-only" for="f1-first-name">Email Address</label>
                <input class="form-control" type="email" name="email-validate" placeholder="Email Address" required>
                <div class="f1-buttons-custom">
                    <button class="btn btn-next" name="submit" type="submit">Validate</button>
                </div>
                </div>
            </div>
            
        </form>
        <div class="success">
            <h2>Great!</h2>
            <h3>Please check your email and confirm your email address</h3>
            <img class="hidden-xs" src="/assets/img/sent-mail.png" width="200" height="150" alt="">
            <p class="hidden-lg hidden-md hidden-sm">&nbsp;</p>
            <p>If you do not see the email in your inbox, please check your spam or junk folder just in case the confirmation email got delivered there.</p>
        </div>     
    </div>  
    <?php } ?>